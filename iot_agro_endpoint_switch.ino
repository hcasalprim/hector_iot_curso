  /*Code By @fenixbinario
    Turn On LED via Web through ESP8266 with Arduino.
    ESP8266 & ARDUINO HTML WEB SERVER.

    more README.md
    */
    
    #include <SoftwareSerial.h>
    #define DEBUG true
    SoftwareSerial esp8266(2,3); // make RX Arduino line is pin 5, make TX Arduino line is pin 6.
     int BOMBILLA=10; //BOMBILLA
     int LED_AZUL=11; //LED AZUL PIN
     int LED_VERDE=12; //LED VERDE PIN
     int LED_ROJO=13; //LED ROJO PIN
     

     int itsONled[] = {0,0,0,0}; // LED STATUS ARRAY eg- ON or OFF at startup. each value represent ech led.
     
    void setup()
    {
      pinMode(BOMBILLA, OUTPUT);
      pinMode(LED_AZUL, OUTPUT);
      pinMode(LED_VERDE, OUTPUT);
      pinMode(LED_ROJO, OUTPUT);
      Serial.begin(9600);
      esp8266.begin(115200); // your esp's baud rate might be different
      sendData("AT+RST\r\n",2000,DEBUG); // reset module
      //sendData("AT+CWMODE=2\r\n",1000,DEBUG); // configure as Access point mode
      //sendData("AT+CWSAP=\"AccessPointname\",\"password\",1,4\r\n",1000,DEBUG); //this sets access point name and password //[channel -> 1 to 10] , [securitytype -> 0 – Open / 2 – WPA_PSK /3 – WPA2_PSK / 4 – WPA_WPA2_PSK ]
      sendData("AT+CWMODE=1\r\n",1000,DEBUG); // configure as Wireless Station mode
      sendData("AT+CWJAP=\"SSID\",\"PASSWORD\"\r\n", 6000, DEBUG); //Put Your SSID and password if activate as Station mode else comment down the line
      sendData("AT+CIFSR\r\n",2000,DEBUG); // get ip address
      sendData("AT+CIPMUX=1\r\n",1000,DEBUG); // configure for multiple connections
      sendData("AT+CIPSERVER=1,3000\r\n",1000,DEBUG); // turn on server on port 3000
    }

void loop()
{
     
  if(esp8266.available()) // check if the esp is sending a message 
  {
    if(esp8266.find("+IPD,"))
    {
     // subtract 48 because the read() function returns 
     // the ASCII decimal value and 0 (the first decimal number) starts at 48
     int connectionId = esp8266.read()-48; 
     //To read the url sent by the client
     String msg;
     esp8266.find("?");
     delay(100);
     msg = esp8266.readStringUntil(' ');
     String command1 = msg.substring(0);
     // HTML START
     String webpage = "<html><head><title>ESP8266 WEB SWITCH</title></head><body>";
     //COMMANDS TO TURN ON or OFF LED RECEIVE BY WEB
                if (command1 == "pin=10"){//if recieve pin=10 from url it will turn on light by checking value in itsONled, if value is 0 it means on, if find 1 it will switch off led.
                    if (itsONled[0] == 1) // if find value 1 it will turn of led
                    {
                      digitalWrite(BOMBILLA, LOW);
                      itsONled[0] = 0; // set value 0
                      webpage += "<p>LIGHT STATUS OFF</p>";
                    }
                    else
                    {// if find value 0 it will turn on led
                      digitalWrite(BOMBILLA, HIGH);
                      itsONled[0] = 1;// set value 1
                      webpage += "<p>LIGHT STATUS ON</p>";
                    }
                }
                else if (command1 == "pin=11"){//if recieve pin=11 from url it will turn BLUE LED by checking value in itsONled, if value is 0 it means on, if find 1 it will switch off led.
                    if (itsONled[1] == 1) // if find value 1 it will turn of led
                    {
                      digitalWrite(LED_AZUL, LOW);
                      itsONled[1] = 0; // set value 0
                      webpage += "<p>BLUE LED STATUS OFF</p>";
                    }
                    else
                    {// if find value 0 it will turn on led
                      digitalWrite(LED_AZUL, HIGH);
                      itsONled[1] = 1;// set value 1
                      webpage += "<p>BLUE LED STATUS ON</p>";
                    }
                }
                else if (command1 == "pin=12"){//if recieve pin=11 from url it will turn GREEN LED by checking value in itsONled, if value is 0 it means on, if find 1 it will switch off led.
                    if (itsONled[2] == 1) // if find value 1 it will turn of led
                    {
                      digitalWrite(LED_VERDE, LOW);
                      itsONled[2] = 0; // set value 0
                      webpage += "<p>GREEN LED STATUS OFF</p>";
                    }
                    else
                    {// if find value 0 it will turn on led
                      digitalWrite(LED_VERDE, HIGH);
                      itsONled[2] = 1;// set value 1
                      webpage += "<p>GREEN LED STATUS ON</p>";
                    }
                }
                else if (command1 == "pin=13"){//if recieve pin=11 from url it will turn RED LED by checking value in itsONled, if value is 0 it means on, if find 1 it will switch off led.
                    if (itsONled[3] == 1) // if find value 1 it will turn of led
                    {
                      digitalWrite(LED_ROJO, LOW);
                      itsONled[3] = 0; // set value 0
                      webpage += "<p>RED LED STATUS OFF</p>";
                    }
                    else
                    {// if find value 0 it will turn on led
                      digitalWrite(LED_ROJO, HIGH);
                      itsONled[3] = 1;// set value 1
                      webpage += "<p>RED LED STATUS ON</p>";
                    }
                }

     webpage += "</body></html>";
     String cipSend = "AT+CIPSEND=";
     cipSend += connectionId;
     cipSend += ",";
     cipSend +=webpage.length();
     cipSend +="\r\n";
     sendData(cipSend,200,DEBUG);
     sendData(webpage,200,DEBUG);
     //BELOW THIS LINE CLOSE THE CONNECTION
     String closeCommand = "AT+CIPCLOSE="; 
     closeCommand+=connectionId; // append connection id
     closeCommand+="\r\n";
     sendData(closeCommand,300,DEBUG);
    }
  }
     
}

//PROGRAM TO SEND COMMAND TO ESP8266 
void sendData(String command, const int timeout, boolean debug)
{
    esp8266.print(command); // send the read character to the esp8266
    long int time = millis();

    while( (time+timeout) > millis())
    {
      while(esp8266.available())
      {
        // The esp has data so display its output to the serial window 
        Serial.write(esp8266.read());
      }  
    }
}